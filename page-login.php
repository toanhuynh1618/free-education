
<?php session_start();?>

<?php 
if (isset($_POST['ok'])) {
     //kiểm tra email
	if (!empty($_POST['emaill'])) {
		$email = $_POST['emaill'];
	}
    //kiểm tra pass
	if (!empty($_POST['pass'])) {
		$password = $_POST['pass'];
	}


$info = array('user_login' => $email, 'user_password' => $password);
    // kiểm tra đăng nhập sai 
$user_signon = wp_signon($info, false);
if (is_wp_error($user_signon)) {
	$loi_login = "* Email hoặc mật khẩu sai !";
} else {	// nếu đăng nhập đúng 
	
	
	$args = array( 
		'user_login' => $email,
	);
	$user_query = new WP_User_Query( $args );
	// User Loop
	if ( ! empty( $user_query->get_results() ) ) {
		foreach ( $user_query->get_results() as $user ) {
			if($user->user_login == $email)
			{
				$display_name =  $user->display_name;
				$_SESSION['display_name']=$display_name;
				echo $_SESSION['display_name'];
			}
		}
	} 
	// Reset Post Data
	wp_reset_postdata();


     // nếu có check remeber me
	if (!empty($_POST["remember"])) {

		setcookie("member_user", $email, time() + (10 * 365 * 24 * 60 * 60));
		setcookie("member_pass", $password, time() + (10 * 365 * 24 * 60 * 60));
        //hàm chuyển trang wp
		$url = get_home_url();
		wp_redirect($url);
		exit();;
	}
      // ngược lại là ko check
	else {
		setcookie("member_user", "");
		setcookie("member_pass", "");
		$url = get_home_url();
		wp_redirect($url);
		exit();
	}

}

}
?>

<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * @package Free_Education
 */

get_header();
?>

<!-- Blog Single -->
<section class="blog b-archives single section">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-12">
				<div id="id01" class="">
					<div class="modal-content animate">
						<form class="wrap-form wrap-login"  action="" method="post">
							<div class="signup-content">
								<div class="signup-image login-image">
									<div class="form-right">
										<figure><img src="http://localhost:8080/wordpress/BSA-education/wp-content/uploads/2019/05/signin-image.jpg" alt="sing up image"></figure>
										<a href="../Wordpress/register" class="signup-image-link"><i class="fa fa-users"></i>&ensp;Đăng ký tài khoản</a>
									</div>
								</div>
								<div class="signup-form login">
									<div>
									<h2 class="form-title">Đăng nhập</h2>
									<form  class="register-form" id="register-form">
										<div class="form-group">
											<label for="name"><i class="fa fa-user"></i></label>
											<input type="email" name="emaill" id="emaill" placeholder="Email"  value="<?php if(isset($_COOKIE["member_user"])) { echo $_COOKIE["member_user"]; } ?>" required>
										</div>
										
										<div class="form-group">
											<label for="pass"><i class="fa fa-lock"></i></label>
											<input type="password" name="pass" id="pass" placeholder="Mật khẩu" value="<?php if(isset($_COOKIE["member_pass"])) { echo $_COOKIE["member_pass"]; } ?>" required>
										</div>

										<div class="form-group">
											<input type="checkbox" name="remember" id="check" <?php if(isset($_COOKIE["member_user"]) && isset($_COOKIE["member_pass"])) { ?> checked <?php } ?>>
											<label id="remember" for="email" style="font-size: 13px">Remember me</label>
										</div>
										
										<div class="form-group form-button">
											<input type="submit" name="ok" id="signup" class="form-submit" value="Đăng Nhập">
										</div>
										<div class="form-group">
											<a href="forgot-password" class="signup-image-link">Quên mật khẩu</a>
										</div>
										<div  style="color:red; text-align:center">
											<?php
												if(!empty('loi_login'))
												{
													echo $loi_login;
												}
											?>
										</div>
									</form>
								</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-12">
				<div class="learnedu-sidebar">
					<?php get_sidebar();?>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/ End Blog Single -->

<?php
get_footer();
