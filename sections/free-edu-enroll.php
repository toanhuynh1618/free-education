<?php $free_education_frontpage_enroll_option = get_theme_mod( 'free_education_frontpage_enroll_option', 'show' );
if( $free_education_frontpage_enroll_option == 'show' ) :?>
	<!-- Enroll -->
	<section class="enroll overlay section" data-stellar-background-ratio="0.5" style="background-image: url('http://localhost:8080/Wordpress/wp-content/uploads/2018/07/slider1.jpg');">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					<div class="row">
						<div class="col-lg-10 col-12 wow fadeInUp" data-wow-delay="0.4s">
							<!-- Single Enroll -->
							<div class="enroll-form">
								<div class="form-title">
									<?php 
									$enroll_form_title = get_theme_mod('free_education_frontpage_enroll_form_title_option');
									$enroll_form_subtitle = get_theme_mod('free_education_frontpage_enroll_form_subtitle_option');
									?>
									<h2><!-- <?php echo esc_html($enroll_form_title);?> -->Liên hệ với chúng tôi</h2>
									<p><!-- <?php echo esc_html($enroll_form_subtitle);?> --></p>
								</div>
								<!-- Form -->
								<form class="form" method="post">
									<?php if (get_theme_mod('free_education_contact_form_code')):
										echo do_shortcode(get_theme_mod('free_education_contact_form_code')); 
									endif; ?>		
								</form>
								<!--/ End Form -->
							</div>
							<!-- Single Enroll -->
						</div>
							
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Enroll -->
	<?php endif;?>		