<?php $free_education_frontpage_service_option = get_theme_mod( 'free_education_frontpage_service_option', 'show' );
if( $free_education_frontpage_service_option == 'show' ) :?>
<!-- Features -->
<section class="our-features section">
	<div class="container wrap-course">
		<div class="row">
			<div class="col-12 wow zoomIn">
				<div class="section-title">
					<?php
					$service_title = get_theme_mod('free_education_frontpage_service_title_option');
					$query_post = get_post($service_title);
					?>
					<h2><?php echo esc_html($query_post->post_title);?></h2>
					<p><?php echo esc_html($query_post->post_content);?></p>
					<?php wp_reset_postdata();?>
				</div>
			</div>
		</div>

		<div class="row">
			<?php
			$args = array( 'post_type' => 'service', 'posts_per_page' => 4 );
              $loop = new WP_Query( $args );
              if ( $loop->have_posts() )
              {
                while ($loop->have_posts()) 
                {
                  $loop->the_post();
              
//                  echo "<div class='col-xs-12 col-sm-6 col-md-3 col-lg-3'>";
//                    
//                      echo "<p style='text-align: center;'>".  the_title()."</p>";
//                      echo "<p>".the_content()."</p>";
//                  echo "</div>";
					echo"<div class='col-lg-3 col-md-3 col-12 wow fadeInUp' data-wow-delay='0.4s'>";
					echo"<div class='single-feature'>";
					if(has_post_thumbnail()):
						echo"<div class='feature-head'>";
							the_post_thumbnail( 'free-education-frontpage-service-image-370x250' );
						echo"</div>";
					endif;
						
							the_title( '<h4 class="blog-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>' );
						$a = 0;
						$a = get_the_excerpt();
						echo"<p>";
							echo _substr($a,230);
						echo "</p>";
						
					echo"</div>";
				echo"</div>";
                }
              }
			?>
	</div>			
		
</div>
</section>
<!-- End Features -->
<?php endif;?>