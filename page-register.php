
<?php 
// kiểm tra xem người dùng đã nhấn signup chưa
if (isset($_POST['signup'])) {

    //kiểm tra tên người dùng
	if (empty($_POST['namee'])) {
		echo "tên người dùng trống";
	} else {
		$name = $_POST['namee'];
	}

    //kiểm tra email
	if (empty($_POST['emaill'])) {
		echo "email trống";
	} else {
		$email = $_POST['emaill'];
	}

    //kiểm tra mật khẩu
	if (empty($_POST['pass'])) {
		echo "pass trống";
	} else {
		$pass = $_POST['pass'];
	}

    //kiểm tra số điện thoại
	if (empty($_POST['numberr'])) {
		echo "number trống";
	} else {
		$number = $_POST['numberr'];
	}

    //chèn dữ liệu vào database
	if(email_exists( $email )){
		$loiemail = "Email đã tồn tại vui lòng đăng ký 1 email khác !";
	}else{
		$userdata = array(
			'user_login' => $email,
			'user_pass' => $pass,
			'user_nicename' => $name,
			'user_email' => $email,
			'display_name' => $name,
			 'role'=>'Subscriber'  // quyền cho user
		);
		$user_id = wp_insert_user($userdata);
		// echo $user_id;

		if (!is_wp_error($user_id)) {
			update_usermeta($user_id, 'phone', $number);
			$url = get_home_url()."/login";
			wp_redirect($url);
			exit();
		}
	}
}

?>
<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * @package Free_Education
 */

get_header();
?>

<!-- Blog Single -->
<section class="blog b-archives single section">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-12">
				<div id="id01" class="">
					<div class="modal-content animate">
						<form class="wrap-form" action="" method="post" onSubmit="return checkPw(this) ">
							<div class="signup-content">
								<div class="signup-form">
									<h2 class="form-title">Đăng ký</h2>
									
										<div class="form-group">
											<label for="name"><i class="fa fa-user"></i></label>
											<input type="text" name="namee" id="namee" placeholder="Tên người dùng" required>
										</div>

										<div class="form-group">
											<label for="email" style="font-size: 13px"><i class="fa fa-envelope"></i></label>
											<input type="email" name="emaill" id="emaill" placeholder="Email" required>
										</div>

										<div class="form-group">
											<label for="pass"><i class="fa fa-lock"></i></label>
											<input type="password" name="pass" id="pass" placeholder="Mật khẩu" required>
										</div>

										<div class="form-group">
											<label for="re-pass"><i class="fa fa-unlock-alt"></i></label>
											<input type="password" name="re_pass" id="re_pass" placeholder="Nhập lại mật khẩu" required>
										</div>

										<div class="form-group">
											<label for="number" style="font-size: 24px"><i class="fa fa-mobile"></i></label>
											<input type="number" name="numberr" id="numberr" placeholder="Số điện thoại" required>
										</div>

										<div class="form-group form-button">
											<input type="submit" name="signup" id="signup" class="form-submit" value="Đăng ký">
										</div>
										<div style="color:red">
											<?php 
												if(!empty($loiemail))
												{
													echo '* '. $loiemail;
												}
											?>
										</div>
									
								</div>
								<div class="signup-image">
									<div class="form-right">
										<figure><img src="http://localhost:8080/wordpress/BSA-education/wp-content/uploads/2019/05/signup-image.jpg" alt="sing up image"></figure>
										<a href="login" class="signup-image-link"><i class="fa fa-users"></i>&ensp;Đã có tài khoản</a>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-12">
				<div class="learnedu-sidebar">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/ End Blog Single -->
<script LANGUAGE="JavaScript">
function checkPw(form) {
  pw1 = form.pass.value;
  pw2 = form.re_pass.value;
  if (pw1 != pw2) {
    swal({
      title: "vui lòng xác nhận lại mật khẩu !",
      icon: "error",
    });
    return false;
  }
  else return true;
}
// End -->
</script>
<?php
get_footer();
?>