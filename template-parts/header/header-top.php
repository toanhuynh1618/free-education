<?php 
$free_education_top_header_option = get_theme_mod( 'free_education_top_header_option', 'show' );
if( $free_education_top_header_option == 'show' ) :?>
	<!-- Topbar -->
	<div class="topbar">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-12">
					<?php 
					$free_education_top_header_social_option = get_theme_mod( 'free_education_top_header_social_option', 'show' );
					if( $free_education_top_header_social_option == 'show' ) :?>

						<?php free_education_social_media_items();?>

					<?php endif;?>

				</div>
				<div class="col-lg-4 col-12">
					<!-- <?php free_education_top_header_items();?>-->

					<div class="content">
					<?php
						if(isset($_SESSION['display_name']))
						{?>
						<a href="logout"><i class="fa "></i>
							Đăng xuất
						</a>
						&emsp;
						<a href="login"><i class="fa fa-user-circle"></i>
							<?php echo $_SESSION['display_name'];?>
						</a>
					<?php }
						else{
					?>
					
					
						<a href="register"><i class="fa fa-user-plus"></i>
							Đăng ký
						</a>
						&emsp;
						<a href="login"><i class="fa fa-user-circle"></i>
							Đăng nhập
						</a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- End Topbar -->
<?php endif;?>