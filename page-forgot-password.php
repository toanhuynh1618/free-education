<?php $get_id = $_GET['id']?>
<?php
// form xác nhận mật khẩu
if(isset($_POST['change']))
{
	//kiểm tra pass2 có tồn tại hay ko
    if(!empty($_POST['pass2']))
    {
      $pass2 = $_POST['pass2'];
	}
	//update pass
	$user_id = $get_id;
	$user_id = wp_update_user( array( 'ID' => $user_id, 'user_pass' => $pass2 ) );

	//chuyển sang trang login
	$url = get_home_url()."/login/";
		wp_redirect($url);
		exit();
}

?>
<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * @package Free_Education
 */

get_header();
?>

<?php
// form quên mật khẩu
if (isset($_POST['ok'])) 
{
	if (!empty($_POST['emaill'])) 
	{
		$email = $_POST['emaill'];
	}
	//gửi mail
	$subject = "title mail 123";
	$id = email_exists($email);
	$message = "Click vào link sau để thay đổi mật khẩu : http://localhost:8080/wordpress/BSA-education/forgot-password/?id=$id";

	if ($id > 0) 
	{
		
		wp_mail($email, $subject, $message);
		?>
		<script>
			swal({
			title: "Vui lòng kiểm tra Email để thay đổi mật khẩu !",
			icon: "success",
			});
		</script>
		<?php
	} else {
		?>
		<script>
			swal({
			title: "Email không tồn tại !",
			icon: "error",
			});
		</script>
		<?php
	}

}
?>

<!-- Blog Single -->
<section class="blog b-archives single section">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-12">
				<div id="id01" class="">
					<div class="modal-content animate">
						<form  action="" method="post" class="wrap-form wrap-login"  onSubmit="return checkPw(this)">
							<div class="signup-content">
								<div class="signup-image login-image">
									<div class="form-right">
										<figure><img src="http://localhost:8080/wordpress/BSA-education/wp-content/uploads/2019/05/key.png" alt="sing up image"></figure>
										<a href="login" class="signup-image-link"><i class="fa fa-users"></i>&ensp;Đăng nhập</a>
									</div>
								</div>
								<div class="signup-form login">
									<div>
									
									<?php if (!isset($get_id)){?>
										<h2 class="form-title">Quên Mật Khẩu</h2>
										<form class="register-form" id="register-form">	
										<div class="form-group">
											<label for="name"><i class="fa fa-user"></i></label>
											<input type="email" name="emaill" id="name" placeholder="Email" required>
										</div>

										<div class="form-group form-button">
											<input type="submit" name="ok" id="ok" class="form-submit" value="Xác Nhận">
										</div>

									<?php } else{?>
										<h2 class="form-title">Thay đổi mật khẩu</h2>
										<form class="register-form" id="register-form">	
										<div class="form-group">
											<label for="name"><i class="fa fa-user"></i></label>
											<input type="password" name="pass1" id="name" placeholder="Mật khẩu mới" required>
										</div>

										<div class="form-group">
											<label for="name"><i class="fa fa-user"></i></label>
											<input type="password" name="pass2" id="name" placeholder="Xác nhận mật khẩu" required>
										</div>

										<div class="form-group form-button">
											<input type="submit" name="change" id="change" class="form-submit" value="Xác Nhận">
										</div>
									<?php }?>
									</form>
								</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-12">
				<div class="learnedu-sidebar">
					<?php get_sidebar();?>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/ End Blog Single -->

<SCRIPT LANGUAGE="JavaScript">
function checkPw(form) {
  pw1 = form.pass1.value;	
  pw2 = form.pass2.value;
  
  if (pw1 != pw2) {
    swal({
      title: "Xác nhận lại mật khẩu chưa đúng !",
      icon: "error",
    });
    return false;
  }
  else 
  {
	swal({
      title: "Thành công !",
      icon: "success",
    });
	return true;
  }
}
// End -->
</script>
<?php
get_footer();
